let info = document.querySelector('#info')
let cells = document.querySelectorAll('#field td');
let gamers = ['X', 'O'];
start(cells);

function start(cells) {
   let i = 0;
   info.textContent = `Ходит игрок ${'X'}`

   for (let cell of cells) {
      cell.addEventListener('click', function step() {
         this.textContent = ['X', 'O'][i % 2];
         this.removeEventListener('click', step);

         if (this.textContent == 'X') {
            info.textContent = `Ходит игрок ${'O'}`
         } else {
            info.textContent = `Ходит игрок ${'X'}`
         }

         if (isVictory(cells)) {
            document.body.innerHTML = `<div id="res">Победил игрок ${this.textContent}</div>`
            console.log(`Победил игрок ${this.textContent}`);
         } else if (i == 8) {
            document.body.innerHTML = `<div id="res">Игроки сыграли в ничью</div>`
            console.log('Игроки сыграли в ничью');
         }
         i++;
      });
   }
}

function isVictory(cells) {
   let combs = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
   ];

   for (let comb of combs) {
      if (
         cells[comb[0]].textContent == cells[comb[1]].textContent &&
         cells[comb[1]].textContent == cells[comb[2]].textContent &&
         cells[comb[0]].textContent != ''
      ) {
         return true;
      }
   }

   return false;
}